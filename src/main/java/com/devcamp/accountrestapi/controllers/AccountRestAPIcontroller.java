package com.devcamp.accountrestapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.accountrestapi.models.Account;

@RestController
@RequestMapping("/")
@CrossOrigin
public class AccountRestAPIcontroller {
    @GetMapping("/account")
    public ArrayList<Account> accountList(){
        ArrayList<Account> accountList = new ArrayList<>();
        Account account1 = new Account("1", "minh");
        Account account2 = new Account("2", "phuong", 1000);
        Account account3 = new Account("3", "an", 1000);
        System.out.println(account1.toString());
        System.out.println(account2.toString());
        System.out.println(account3.toString());

        accountList.add(account1);
        accountList.add(account2);
        accountList.add(account3);

        return accountList;
    }
}
